package safira_shared_kernel

import (
	"testing"
)

func Test_MoneyValueToString(t *testing.T) {

	money1 := NewMoney(932.97)

	if money1.ToString() != "932.970000" {
		t.Fatalf("Valor inesperado para o input [%v] != 932.970000", money1.ToString())
	}

	money2 := NewMoney(0.097)
	if money2.ToString() != "0.097000" {
		t.Fatalf("Valor inesperado para o input [%v] != 0.097000", money2.ToString())
	}
}
