package safira_shared_kernel

import (
	"fmt"
	"math"
)

/* ========== Decimal ========== */
type Decimal struct {
	bigValue  int64
	precision uint8
}

func (d Decimal) factor() float64 {
	return math.Pow10(int(d.precision))
}

func NewDecimal(value float64, precision uint8) Decimal {
	d := Decimal{precision: precision}

	d.bigValue = int64(value * d.factor())
	return d
}

func (d Decimal) ToString() string {
	if d.bigValue > int64(d.factor()) {
		v := fmt.Sprint(d.bigValue)
		breakPoint := len(v) - int(d.precision)

		return fmt.Sprintf("%v.%v", v[:breakPoint], v[breakPoint:])
	}

	return fmt.Sprintf("0.%v", fmt.Sprint(d.bigValue + int64(d.factor()))[1:])
}

func (d Decimal) ToFloat() float64 {
	return float64(d.bigValue) / d.factor()
}

func (d Decimal) Mult(value Decimal) Decimal {
	result := Decimal{d.bigValue * value.bigValue, d.precision + value.precision}

	return result.ChangePrecision(d.maxPrecision(value))
}

func (d Decimal) Plus(value Decimal) Decimal {
	newPrecision := d.maxPrecision(value)
	d1 := d.ChangePrecision(newPrecision)
	d2 := value.ChangePrecision(newPrecision)

	return Decimal{d1.bigValue + d2.bigValue, newPrecision}
}

func (d Decimal) Minus(value Decimal) Decimal {
	return d.Plus(value.Negative())
}

func (d Decimal) Negative() Decimal {
	return Decimal{-d.bigValue, d.precision}
}

func (d Decimal) maxPrecision(value Decimal) uint8 {
	return uint8(math.Max(float64(d.precision), float64(value.precision)))
}

func (d Decimal) ChangePrecision(newPrecision uint8) Decimal {
	diff := int(newPrecision) - int(d.precision)
	newBigValue := float64(d.bigValue) * math.Pow10(diff)
	return Decimal{int64(newBigValue), newPrecision}
}
