package safira_shared_kernel

import (
	"errors"
	"time"
)

type Date struct {
	value string
}

var ErrDateFormat = errors.New("@DateFormat")
var ZeroDateValue = Date{}

const date_layout = "2006-01-02"

func NewDate(value string) (Date, error) {

	if len(value) < 10 {
		return ZeroDateValue, ErrDateFormat
	}

	t, err := time.Parse(date_layout, value[0:10])

	if err != nil {
		return ZeroDateValue, ErrDateFormat
	}

	return Date{t.Format(date_layout)}, nil
}

func NewDateNow() Date {
	return Date{time.Now().Format(date_layout)}
}
