package safira_shared_kernel

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewDate_WithValidArguments_Success(t *testing.T) {

	date_with_seconds := "2021-12-13"

	_, err := NewDate(date_with_seconds)

	assert.Nil(t, err)
}

func Test_NewDate_WithSeconds_Success(t *testing.T) {
	date_with_seconds := "2021-12-13T23:29:09Z"
	expectedDate, _ := NewDate("2021-12-13")

	result, err := NewDate(date_with_seconds)

	assert.Nil(t, err)
	assert.Equal(t, expectedDate, result)
}

func Test_NewDate_InvalidDate_ReturnError(t *testing.T) {
	const (
		invalid_date = "2021-01"
	)

	_, err := NewDate(invalid_date)

	assert.NotNil(t, err)
}

func Test_NewDate_InvalidIsoFormatDate_ReturnError(t *testing.T) {
	const invalid_iso_format_date = "202-17-13T02:34:58"

	_, err := NewDate(invalid_iso_format_date)

	assert.NotNil(t, err)
}
