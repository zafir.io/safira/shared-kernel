package safira_shared_kernel

/* ========== Percentage ========== */
type Percentage struct {
	Decimal
}

func NewPercentage(value float32) Percentage {

	return Percentage{NewDecimal(float64(value), 2)}
}

func (p Percentage) Of(value Decimal) Decimal {
	result := p.Mult(value)

	return Decimal{result.bigValue / 100, result.precision}
}

func (p Percentage) From(value Decimal) Decimal {
	toSubtract := p.Of(value)

	return value.Minus(toSubtract)
}
