package safira_shared_kernel

import (
	"errors"
	"regexp"
)

var ErrIdFormat = errors.New("@IdFormat")

type Id struct {
	value string
}

func (i *Id) Value() string {
	return i.value
}

func NewId(rawId string) (Id, error) {

	if matched, err := regexp.MatchString("^[A-Za-z0-9]{8}$", rawId); !matched || err != nil {
		return Id{}, ErrIdFormat
	}

	return Id{rawId}, nil
}
