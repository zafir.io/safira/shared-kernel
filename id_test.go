package safira_shared_kernel

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewId_Success(t *testing.T) {
	// Arrange
	validRawId := "dsper5pq"

	//  Act
	id, err := NewId(validRawId)

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, id.Value(), validRawId)
}

func TestNewId_InvalidIdFormat(t *testing.T) {
	// Arrange
	invalidIds := []string{"dQ1l93.d", "da4snrP", "da4snrPw2"}
	// Test
	testNewIdsArray_InvalidFormat(invalidIds, t)
}

func testNewIdsArray_InvalidFormat(ids []string, t *testing.T) {
	//  Act
	_, err := NewId(ids[0])
	// Assert
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), ErrIdFormat.Error())

	if len(ids) > 1 {
		testNewIdsArray_InvalidFormat(ids[1:], t)
	}
}
