package safira_shared_kernel

/* ========== Money ========== */
type Money struct {
	Decimal
}

func NewMoney(value float64) Money {
	return Money{NewDecimal(value, 6)}
}

func NewMoneyValueFromDecimal(value Decimal) Money {
	return NewMoney(float64(value.bigValue) / value.factor())
}

func (m Money) Mult(value Money) Money {
	return NewMoney(m.Decimal.Mult(value.Decimal).ToFloat())
}

func (m Money) Plus(value Money) Money {
	return NewMoney(m.Decimal.Plus(value.Decimal).ToFloat())
}

func (m Money) Minus(value Money) Money {
	return NewMoney(m.Decimal.Minus(value.Decimal).ToFloat())
}
